package ru.aushakov.tm.bootstrap;

import ru.aushakov.tm.api.ICommandController;
import ru.aushakov.tm.api.ICommandRepository;
import ru.aushakov.tm.api.ICommandService;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.controller.CommandController;
import ru.aushakov.tm.repository.CommandRepository;
import ru.aushakov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAppInfo();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                showArgumentWarning();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                commandController.showAppInfo();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            default:
                showCommandWarning();
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        if (args.length > 1) System.out.println("NOTE: Only one argument is supported at a time!");
        return true;
    }

    public void showCommandWarning() {
        System.out.println("The command entered is not supported!");
    }

    public void showArgumentWarning() {
        System.out.println("The argument entered is not supported!");
    }

}
