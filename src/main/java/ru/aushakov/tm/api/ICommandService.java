package ru.aushakov.tm.api;

import ru.aushakov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
